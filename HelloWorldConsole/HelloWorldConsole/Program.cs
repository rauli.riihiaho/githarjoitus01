﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //Tulostaa teksin
            Console.WriteLine("Hello Git");

            Laskin laskin1 = new Laskin();
            int summa = laskin1.Summa(5, 6);
            Console.WriteLine("Summa: " + summa);
        }
    }
}
